# Name: Jared Diamond
# Assignment: HW5
# Class: CS 454
# Professor: Ben McCamish
#
# Description:
# Reducer.py takes 3 column lines of input from stdin (supplied by mapper.py), sums up columns 2 and 3 for every like tag found in column 1
# and then prints it's formatted output to stdout.
# It's meant to be used with both mapper.py and hadoop. It's output should be redirected to a file.
 
import sys
from operator import itemgetter

current_tag = None
current_size = 0
current_visits = 0
tag = None

for line in sys.stdin:
    # incoming line cleaned up and split on tab
    line = line.strip()
    tag, visits, size = line.split('\t')

    # visits and size converted to ints
    visits = int(visits)
    size = int(size)

    # sum up current tags visits/size counts until a new tag is found
    if current_tag == tag:
        current_visits += visits
        current_size += size
    else:
        # print out current tags visits/size before starting a new tag
        if current_tag:
            print(f"{current_tag}" + "{size}\t" + f"{current_size}")
            print(f"{current_tag}" + "{visits}\t" + f"{current_visits}")

        # all current variables updated
        current_tag = tag
        current_visits = visits
        current_size = size

# last tag printed out to stdout
if current_tag == tag:
    print(f"{current_tag}" + "{size}\t" + f"{current_size}")
    print(f"{current_tag}" + "{visits}\t" + f"{current_visits}")

