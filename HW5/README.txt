Name: Jared Diamond
Assignment: HW5
Class: CS 454
Professor: Ben McCamish

Files contained in zip:
	output.txt	The mapped and reduced output of 'input.dat'
	mapper.py	A mapper program that can be used by hadoop
	reducer.py	A reducer program that can be used by hadoop

Run instructions:

To run map/reduce type the following command into a terminal:

	cat input.dat | python3 mapper.py | sort | python3 reducer.py > output.txt

