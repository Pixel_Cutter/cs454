# Name: Jared Diamond
# Assignment: HW5
# Class: CS 454
# Professor: Ben McCamish
#
# Description:
# Mapper.py takes a 4 column input file from stdin and outputs all lines that contain 'T' in the final field, leaving out the 'T'
# It's intended to be used with hadoop
 
import sys

for line in sys.stdin:
    # incoming line cleaned up and split on space
    newLine = line.strip()
    split = line.split()

    # if last field of input line == 'T': print first 3 fields to stdout
    if split[-1] == 'T':
        print(f"{split[0]}\t{split[1]}\t{split[2]}")
