import math
import csv
import re

class BM_25(object):
	"""
	A class used to rank documents based on a query. Ranking is done via the BM25 ranking algorithm.
	
	Attributes
	----------
	corpus : dictionary
		A dictionary with two fields: document and document id. BM_25 expects a clean/formatted csv in order to
		build the corpus

	docCount : int
		The total number of documents found in the corpus

	avgdl : float
		The average length (in words) of all documents present in the corpus

	Methods
	-------
	bm25(query, k) : list
		Constructs a list of the top k ranked documents and then returns it

	bmScore(doc, terms) : float
		Calculates a documents rank based on a list of terms supplied

	ft(d, t) : int
		Called from bmScore(). Returns the amount of times a term (t) occurs in a document (d)

	dl(d) : int
		Called from bmScore(). Returns the length (in words) of a document (d)

	qft(t, Q) : int
		Called from bmScore(). Returns the amount of times a term (t) occurs in a query (Q)

	dft(term) : int
		Called from bmScore(). Returns the amount of documents that contain a term (term)

	findDocs(terms) : None
		Called from bm25(). Loops through corpus and searches for documents that contain a term of a query.
		When a document is found, two things happen: the doc id is appended to a list containing all the 
		documents query words were found in (termDict['docList']), and it appends the doc id to a list associated
		with that specific term (termDict[term].list).
	"""
	def __init__(self, dataFile):
		self.corpus = {}
		self.docCount = 0
		self.avgdl = 0

		# loops through dataFile's content and builds corpus dictionary consisting of two fields: id and description.
		# id contains the id of the current document (line) and description contains the current document's content.
		# The description column is cleaned up before being added to corpus

		with open(dataFile, 'r') as fd:
			headers = fd.readline().rstrip().split(',');
			ID = headers.index('id')
			desc = headers.index('description')

			reader = csv.reader(fd, delimiter = ',', quotechar = '"')
		
			totdl = 0

			for line in reader:
				self.corpus[line[ID]] = re.sub(r'[.,!;:&#$\']', '', line[desc]).rstrip()
				self.docCount += 1
				totdl += len(self.corpus[line[ID]].split(' '))

		self.avgdl = totdl/self.docCount

	def bm25(self, query, k) -> list:
		terms = query.split(' ')

		# termDict holds a master list of all the doc id's a queries terms were found in, and
		# lists of doc id's associated with specific terms of a query. These specific lists are 
		# used to quickly determine how many documents contain a term

		self.termDict = {}
		self.termDict['docList'] = []
		self.scoreList = []
		finalList = []

		self.findDocs(terms) # termDict built

		# start ranking the documents present in master list

		for doc in self.termDict['docList']:
			self.scoreList.append((doc, self.bmScore(doc, terms)))

		# sort ranked list in descending order

		self.scoreList.sort(key = lambda x: x[1], reverse = True)

		if k > len(self.scoreList):
			k = len(self.scoreList)

		for i in range(k):
			finalList.append(self.scoreList[i])

		return finalList 
	
	def bmScore(self, doc: str, terms: list) -> float:
		score = 0
		b = 0.75
		k1 = 1.2
		k2 = 500
		N = self.docCount
		dl = self.dl(doc)
		avgdl = self.avgdl
		
		for term in terms:
			# catching key error if term not in termDict
			if term not in self.termDict:
				continue
			
			dft = self.dft(term)
			qft = self.qft(term, terms)
			ft = self.ft(doc, term)
			idf = math.log((N-dft+0.5)/(dft+0.5))			# idf portion of BM25 algorithm
			tf = (((k1+1)*ft)/((k1*(1-b)+b*(dl/avgdl))+ft))	# term frequency portion of BM25 algorithm
			qtf = ((k2+1)*qft)/(k2+qft)						# query term frequency portion of BM25 algorithm

			score += idf*tf*qtf # final rank calculated

		return score

	def ft(self, d: str, t: str) -> int:
		return self.corpus[d].split(' ').count(t)

	def dl(self, d: str) -> int:
		return len(self.corpus[d].split(' '))

	def qft(self, t: str, Q: str) -> int:
		return Q.count(t)

	def dft(self, term: str) -> int:
		return len(self.termDict[term])

	def findDocs(self, terms: list) -> None:
		for term in terms:
			inDocs = [] # list of doc id's associated with term in termDict

			for doc in self.corpus:
				if term in self.corpus[doc].split(' '):
					inDocs.append(doc) # term was found in a document, append doc id to its associated termDict list

					if doc not in self.termDict['docList']:
						self.termDict['docList'].append(doc)

			# if query term was found in the corpus: add it and its associated list to termDict 
			if len(inDocs) != 0:
				self.termDict[term] = inDocs
