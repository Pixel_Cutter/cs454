import math
import re
import csv

class TF_IDF(object):
	"""
	A class used to rank documents based on a query. Ranking is done via the TF-IDF ranking algorithm

	Attributes
	----------
	corpus : dictionary
		A dictionary with two fields. TF_IDF expects a csv that contains a document id field,
		and a document content field as input.
		
	termDict : dictionary
		A dictionary that will eventually hold a master list of document id's that a queries terms
		were found in, and individual terms of a query with an associated list of document ids
		containing the specific term.

	Methods
	-------
	tfidf(Q, k) : list
		Constructs a list of the top k ranked documents and returns it

	relevance(d, Q) : float
		Called either from tfidf, or on its own. Returns the rank of a document (d) based on a query (Q)

	tf(d, t) : float
		Called either from relevance() or on its own. Returns the log of 1 + the ratio of the amount times a query term (t)
		shows up in a document (d) to the length (in words) of the document

	getNt(term) : int
		Called from relevance(). Returns the total amount of documents a term (term) was found in

	addTerm(t) : None
		Called from either tfidf() or relevance(). Loops through corpus of documents and adds document id's
		of documents a term (t) was found in to termDict's master list and that terms specific document id list 
		found in termDict.
	"""
	
	def __init__(self, dataFile):
		self.corpus = {}
		self.termDict = {}
		self.termDict['docList'] = []

		# loops through dataFile's content and builds corpus dictionary consisting of two fields: id and description.
		# id contains the id of the current document (line) and description contains the current document's content.
		# The description column is cleaned up before being added to corpus

		with open(dataFile, 'r') as fd:
			headers = fd.readline().rstrip().split(',');
			ID = headers.index('id')
			desc = headers.index('description')

			reader = csv.reader(fd, delimiter = ',', quotechar = '"')
			
			for line in reader:
				self.corpus[line[ID]] = re.sub(r'[.,!;:&#$\']', '', line[desc]).rstrip()

	def tfidf(self, Q: str, k: int) -> list:
		terms = Q.lower().split(' ')
		self.scoreList = []

		# add the queries terms to termDict

		for term in terms:
			self.addTerm(term)

		# build list of document rankings

		for doc in self.termDict['docList']:
			self.scoreList.append((doc, self.relevance(doc, Q)))
		
		# list of scores sorted and the top k are added to a final list

		self.scoreList.sort(key = lambda x: x[1], reverse = True)

		finalList = []
		
		if k > len(self.scoreList):
			k = len(self.scoreList)

		for i in range(k):
			finalList.append(self.scoreList[i])

		return finalList # final list returned to program that called it

	def relevance(self, d: str, Q: str) -> float:
		totScore = 0 # holds summation of document relevance score
		terms = Q.lower().split(' ')

		for term in terms:
			if term not in self.termDict:
				self.addTerm(term)
			
			# catching dict key error if term is not in termDict
			if term not in self.termDict:
				continue
			
			totScore += self.tf(d, term)/self.getNt(term) # relevance score = sum((term frequency)/(num documents term was found in))
		
		return totScore

	def tf(self, d: str, t: str) -> float:
		totTerms = len(self.corpus[d].split(' '))		# length of document (in words)
		termCount = self.corpus[d].split(' ').count(t)	# the amount of times a term appears in a document
		return math.log(1 + (termCount/totTerms))		# the tf portion of tf-idf

	def getNt(self, term: str) -> int:
		return len(self.termDict[term])
	
	def addTerm(self, t: str) -> None:
		inDocs = [] # list of doc id's associated with term in termDict

		for doc in self.corpus:
			if t in self.corpus[doc].split(' '):
				inDocs.append(doc) # term was found in a document, append doc id to its associated termDict list

				if doc not in self.termDict['docList']:
					self.termDict['docList'].append(doc)
		# if query term was found in the corpus: add it and its associated list to termDict
		if len(inDocs) != 0:
			self.termDict[t] = inDocs
