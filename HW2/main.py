from rank_methods import *

def main():

	tfidf_corpus = TF_IDF.TF_IDF("winemag-data_first150k.csv")
	tf = tfidf_corpus.tf("0", "and")
	topK = tfidf_corpus.tfidf("mac", 5)
	relevance = tfidf_corpus.relevance("80491", "mac watson")

	bm_corpus = BM_25.BM_25("winemag-data_first150k.csv")
	bm_topK = bm_corpus.bm25("mac", 5)

	print("TF_IDF('tremendous', 5):")
	for score in topK:
		print("|", score)

	print("relevance(mac watson):", str(relevance))

	print()

	print("BM_25('tremendous', 5):")
	for score in bm_topK:
		print("|", score)

main()

"""
	index = {}

	with open("wine.csv") as f:
		headers = f.readline().rstrip().split(',');
		ID = headers.index('id')
		desc = headers.index('description')

		while True:
			line = f.readline().rstrip().split(',')

			if len(line) < len(headers):
				break
			index[line[ID]] = line[desc]

	wordIndex = {}

	for docid, desc in index.items():
		splitDesc = desc.split(' ')
		for word in splitDesc:
			if word not in wordIndex:
				wordIndex[word] = []
			if docid not in wordIndex[word]:
				wordIndex[word].append(docid)

	for word, List in wordIndex.items():
		print(word + ":", List)

"""
