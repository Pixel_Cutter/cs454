Jared Diamond
HW2
CS 360
Ben McCamish

Files contained in zip:
- rank_methods/	/ directory containing the classes TF_IDF and BM_25 
- HW2.tex		/ writeup source code
- HW2.pdf		/ writeup pdf

There is nothing special that you need to do to run this program. You can import the classes into a directory
a python program by adding 'from rank_methods import *' and creating your class objects with the following
syntax:
	
	tfidf_corpus = TF_IDF.TF_IDF(datafile)
	bm25_corpus = BM_25.BM_25(datafile)

Or you can import them in your own way. I admit that my understanding of python classes and how to import 
their custom packages is limited.
