#!/usr/bin/python3

from bs4 import BeautifulSoup as bs
import requests 
import shutil
import json
import re

# returns the name of a dish after it has been cleaned
# a cleaned name has had bracketed citation tags and trailing whitespace removed
def cleanName(name):
	return name.split('[')[0].rstrip()

def imgLink(src):
	extList = ['.jpg', '.JPG', '.jpeg', '.png', '.PNG']

	for ext in extList:
		for match in re.finditer(ext, src):
			s = match.start()
			e = match.end()

	fileExt = src[s:e]

	protocol = 'https:'
	jpgSplit = src.split(fileExt)
	base = jpgSplit[0]
	rmThumb = base.replace('/thumb', '')

	link = protocol + rmThumb + fileExt
	
	return link

def getImg(imgURL, name):

	header = {'User-Agent': 'FoodBot/1.0'}

	fileExt = '.' + imgURL.split('.')[-1]
	filename = name.replace("'", "") + fileExt
	imgPath = 'imgs/' + filename
	imgPath = imgPath.replace(' ', '_')

	#rawIMG = requests.get(imgURL, stream = True, headers = header)
	#print("url: " + imgURL)
	#print("code: " + str(rawIMG.status_code))

	#if rawIMG.status_code == 200:
		#rawIMG.raw.decode_content = True

		#with open(imgPath, 'wb') as fd:
		#	shutil.copyfileobj(rawIMG.raw, fd)

	return imgPath
	#else:
		#return "None"

def makeDish(name, imgPath, srcPage, identifier, description):
	dish = {
		"name": name,
		"description": description,
		"imgPath": imgPath,
		"srcPage": srcPage,
		"id": identifier
	}

	return dish

def parseList(dishType, imgCol, titleCol, descCol, section):
	baseName = "List_of_"
	cleanType = dishType.replace(' ', '_')
	fullName = baseName + cleanType
	srcPage = "https://en.wikipedia.org/wiki/" + fullName

	URL = "https://en.wikipedia.org/w/api.php?"
	header = {'User-Agent': 'FoodBot/1.0'}
	PARAMS = {
		"action": "parse",
		"format": "json",
		"prop": "text",
		"section": section,
		"page":	str(fullName) 
	}

	page = requests.get(url=URL, params=PARAMS, headers=header)
	data = page.json()

	soup = bs(data['parse']['text']['*'], 'lxml')
	dishes = soup.find_all('tr')

	i = 0
	dishList = []

	for dish in dishes:
		dataPoints = dish.find_all('td')

		if len(dataPoints) > 0:
			imgPath = "None"
			name = cleanName(dataPoints[titleCol].text)
			
			description = dataPoints[descCol].text.rstrip()

			if len(description) == 0:
				continue

			try:
				imgPoint = str(dataPoints[imgCol].find('img').get('src'))
				imgSrc = imgLink(imgPoint)
				imgPath = getImg(imgSrc, name)
			except:
				pass
			
			identifier = cleanType + "_" + str(i)

			dishList.append(makeDish(name, imgPath, srcPage, identifier, description)) 

			i+=1

	return dishList

			

def main():
	
	with open("targets.json", "r") as fd:
		data = json.load(fd)

	with open("dishes.json", "r+") as fd:
		dishData = json.load(fd)

		for target in data["targets"]:
			parsedList = parseList(target["type"], target["imgCol"], target["nameCol"], target["descCol"], target["section"])
			dishData["dish count"] += len(parsedList)
			
			dishData[target["type"]] = parsedList
	
		fd.seek(0) # go to top of file in preperation for write
		json.dump(dishData, fd, indent = 4)
	
	print("item count: " + str(dishData["dish count"]))


#parseList("fermented foods", 1, 0)

main()
