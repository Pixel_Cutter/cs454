import json


with open("targets.json", "r+") as fd:
	data = json.load(fd)
	
	while True:

		dishType = input("type: ")
		
		if dishType == 'q':
			break
		
		imgCol = input("imgCol: ")
		nameCol = input("nameCol: ")
		descCol = input("descCol: ")
		sectionCol = input("section: ")
		
		dish = {
			"type": dishType,
			"imgCol": int(imgCol),
			"nameCol": int(nameCol),
			"descCol": int(descCol),
			"section": int(sectionCol)
		}

		data["targets"].append(dish)

	fd.seek(0)
	json.dump(data, fd, indent = 4)







#print(data)
#print("number of tuples: " + str(len(data["dishes"])))

#for target in data["targets"]:
#	print("type: " + target["type"])
#	print("imgCol: " + str(target["imgCol"]))
#	print("nameCol: " + str(target["nameCol"]))


