from bs4 import BeautifulSoup
import requests
import lxml

response = requests.get("https://en.wikipedia.org/w/api.php?action=query&prop=extracts&format=json&titles=List%20of%20beef%20dishes")

soup = BeautifulSoup(response.text, 'lxml')

urls = soup.find_all("li")

for url in urls:

	item = url.text.split("\\u2013")

	name = item[0]

	print("{")
	print("name: " + name)

	if len(item) > 1:
		desc = item[1]
		print("description: " + desc)
	
	print("}")


