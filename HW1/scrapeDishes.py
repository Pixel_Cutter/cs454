#!/usr/bin/python3

from bs4 import BeautifulSoup as bs
import requests 
import shutil
import json
import re

'''
cleanName returns the name of a dish after it has been cleaned
a cleaned name has had bracketed citation tags and trailing whitespace removed
'''
def cleanName(name):
	return name.split('[')[0].rstrip()

'''
imgLink takes a raw image link scraped from a table and cleans it up so that is can
be used to download an image.
example cleaning:
	 raw link: //upload.wikimedia.org/wikipedia/commons/thumb/1/14/KFC_Chicken_Soup.jpg/122px-KFC_Chicken_Soup.jpg
	 after cleaning: https://upload.wikimedia.org/wikipedia/commons/1/14/KFC_Chicken_Soup.jpg
'''
def imgLink(src):
	
	# The scraped images vary in what file extension they have. This list/loop is used to determine which file format
	# is being used. This helps split the link at the right spot (first instance of file extension), and helps save it
	# in its native format.

	extList = ['.jpg', '.JPG', '.jpeg', '.png', '.PNG']

	# Loop uses python reqular expressions to determine where the file extension begins/ends
	for ext in extList:
		for match in re.finditer(ext, src):
			s = match.start()
			e = match.end()

	fileExt = src[s:e]
	
	protocol = 'https:'
	jpgSplit = src.split(fileExt) # split on file extension
	base = jpgSplit[0] # base of the download link will be the first element after split
	
	# all of the raw links contained this '/thumb' line to indicate that the pictures on the page should be thumbnails
	# by removing this, the program can download the source images 
	rmThumb = base.replace('/thumb', '')

	# final img link built from it's parts
	link = protocol + rmThumb + fileExt
	
	return link


def getImg(imgURL, filename):

	header = {'User-Agent': 'FoodBot/1.0'}

	imgPath = 'imgs/' + filename
	imgPath = imgPath.replace(' ', '_')

	rawIMG = requests.get(imgURL, stream = True, headers = header)

	if rawIMG.status_code == 200:
		rawIMG.raw.decode_content = True

		with open(imgPath, 'wb') as fd:
			shutil.copyfileobj(rawIMG.raw, fd)

		return imgPath
	else:
		return "None"

def makeDish(name, imgPath, srcPage, identifier, description):
	dish = {
		"name": name,
		"description": description,
		"imgPath": imgPath,
		"srcPage": srcPage,
		"id": identifier
	}

	return dish

def parseList(dishType, imgCol, titleCol, descCol, section):
	baseName = "List_of_"
	cleanType = dishType.replace(' ', '_')
	fullName = baseName + cleanType
	srcPage = "https://en.wikipedia.org/wiki/" + fullName

	URL = "https://en.wikipedia.org/w/api.php?"
	header = {'User-Agent': 'FoodBot/1.0'}
	PARAMS = {
		"action": "parse",
		"format": "json",
		"prop": "text",
		"section": section,
		"page":	str(fullName) 
	}

	page = requests.get(url=URL, params=PARAMS, headers=header)
	data = page.json()

	soup = bs(data['parse']['text']['*'], 'lxml')
	dishes = soup.find_all('tr')

	i = 0
	dishList = []
	for dish in dishes:
		dataPoints = dish.find_all('td')

		if len(dataPoints) > 0:
			imgPath = "None"
			name = cleanName(dataPoints[titleCol].text)
		
			try:
				description = dataPoints[descCol].text.rstrip()
			except:
				continue
			
			if len(description) == 0:
				continue

			imgPoint = "None"
			try:	
				imgPoint = str(dataPoints[imgCol].find('img').get('src'))
				imgSrc = imgLink(imgPoint)
				imgPath = getImg(imgSrc, name)
			except:
				pass
			
			identifier = cleanType + "_" + str(i)

			dishList.append(makeDish(name, imgPath, srcPage, identifier, description)) 

			i+=1

	return dishList

			

def main():
	
	with open("targets.json", "r") as fd:
		data = json.load(fd)

	with open("dishes.json", "r+") as fd:
		dishData = json.load(fd)

		for target in data["targets"]:
			Type = target["type"]
			parsedList = parseList(target["type"], target["imgCol"], target["nameCol"], target["descCol"], target["section"])
			dishData["dish count"] += len(parsedList)
			
			dishData[target["type"]] = parsedList
	
		# go to top of file in preperation for write
		fd.seek(0) 
		# "prettify" output by specifying indent
		json.dump(dishData, fd, indent = 4)
	


main()
