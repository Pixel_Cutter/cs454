import math

class Ranking(object):

    def __init__(self, judgement_file):
        self.judgements = {}

        with open(judgement_file, 'r') as fd:
            for line in fd:
                tmp = line.split()
                query = tmp[0]
                url = tmp[1]
                judgement = tmp[2]

                if query not in self.judgements:
                    self.judgements[query] = {}

                self.judgements[query][url] = int(judgement)


    def prec(self, query_line: str, thresh: int) -> float:
        
        pairList = self.parseQ(query_line)
        query = pairList.pop(0)
        relCount = self.relCount(query, pairList, thresh)

        # -1 to account for query taking up the first pairList index
        return relCount/len(pairList)


    def recall(self, query_line: str, thresh: int) -> float:
       
        pairList = self.parseQ(query_line)
        query = pairList.pop(0)
        rel = self.relCount(query, pairList, thresh)
        total = 0

        for url in self.judgements[query]:
            if self.judgements[query][url] >= thresh:
                total += 1

        if total == 0:
            return 0
        else:
            return rel/total

    def rr(self, query_line: str, thresh: int) -> float:
            
        pairList = self.parseQ(query_line)
        query = pairList.pop(0)
        i = 1
        pos = 0

        if query not in self.judgements:
            return pos

        for url in pairList:
            if url in self.judgements[query]:
                judgement = self.judgements[query][url]
            else:
                continue
            
            if judgement >= thresh:
                pos = i
                return 1/pos
            
            i += 1

        return pos

    def f1_score(self, query_line: str, thresh: int) -> float:

        alpha = 0.5

        try:
            invRecall = 1/self.recall(query_line, thresh)
            invPrec = 1/self.prec(query_line, thresh)
        except:
            return 0
        
        score = 1/(alpha*invPrec + (1 - alpha)*invRecall)

        return score

    def dcg(self, pairList: list, query: str) -> float:        
        
        i = 1
        sumList = []
        
        for url in pairList:
            if url in self.judgements[query]:
                rel  = self.judgements[query][url]
                denom = math.log2(i + 1)
                sumList.append(rel/denom)

            i += 1

        if len(sumList) != 0:
            return sum(sumList)
        else:
            return [0]

    def idcg(self, k: int, query: str) -> float:        
        
        judgeList = []
        i = 1
        total = 0

        for url in self.judgements[query]:
            judgeList.append(self.judgements[query][url])
            
        judgeList.sort(reverse = True)

        for judgement in judgeList[:k]:
            rel = judgement
            denom = math.log2(i + 1)
            total += rel/denom

            i += 1

        return total

    def ndcg(self, query_line: str) -> float:

        pairList = self.parseQ(query_line)
        query = pairList.pop(0)
        
        if query not in self.judgements:
            return 0
        
        k = len(pairList)
        dcg = self.dcg(pairList, query)
        idcg = self.idcg(k, query)
        
        if idcg == 0:
            return 0
        
        return dcg/idcg

    def parseQ(self, query_line: str) -> list:
        tmp = query_line.split()
        query = tmp[0]
        pairList = [query]

        # urls start at index 3 and end at index 13
        for url in tmp[3:13]:
            pairList.append(url)
        
        return pairList

    def relCount(self, query: str, pairList: list, thresh: int) -> int:

        relCount = 0

        if query not in self.judgements:
            return relCount

        for url in pairList:
            if url in self.judgements[query] and self.judgements[query][url] >= thresh: 
                relCount += 1

        return relCount

