#!/bin/python3

import ranking_methods.Ranking as rank

def main():
    ''' main docstring '''
    test = rank.Ranking("relevance_judgements-v1.txt")

    qline = 1
    thresh = 2 

    with open("sample.txt", 'r') as fd:
        for line in fd:
            query = line
        
            prec = test.prec(query, thresh)
            recall = test.recall(query, thresh)
            f1 = test.f1_score(query, thresh)
            rr = test.rr(query, thresh)
            ndcg = test.ndcg(query)

            print(f"Line: {qline} Thresh: {thresh}")
            print("Prec: %.6f" % prec)
            print("Recall: %.6f" % recall)
            print("F1: %.6f" % f1)
            print("RR: %.6f" % rr)
            print("NDCG: %.6f" % ndcg)
            print()

            qline += 1


main()
