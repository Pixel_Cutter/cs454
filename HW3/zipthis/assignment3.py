import math

"""
Name: Jared Diamond
HW: 3
Class: CS454
Professor: Ben McCamish
"""

class Ranking(object):
    """
    Ranking is a class designed to evaluate the user satisfaction score of a given
    set of urls returned from a query. The methods in this class were designed to
    work with a specific yahoo dataset, but they should work with other datasets after
    a little tweaking. 

    Attributes
    ----------
    judgements : dictionary
                A two dimensional dictionary that holds the data contained in 'judgement_file.'
                Dictionary entries follow the following format:
                { query: { url1: judgement_score
                           url2: judgement_score
                           urln: judgement_score }

    Methods
    -------
    prec(query_line: str, thresh: int) : float
        Returns the calculated  precision satisfaction score of a returned set of docs 
        from a query (rel_returned_count/total_returned_count)
    
    recall(query_line: str, thresh: int) : float
        Returns the calculated recall satisfaction score of a returned set of docs
        from a query (rel_returned_count/total_rel_count_in_db)

    rr(query_line: str, thresh: int) : float
        Returns the calculated rr satisfaction score of a returned set of docs from a
        query (1/position_of_first_rel_doc)

    f1_score(query_line: str, thresh: int) : float
        Returns the calculated f1 satisfaction score of a returned set of docs from a 
        query (see method for formula)

    dcg(pairList: list, query: str) : float
        Returns the calculated dcg satisfaction score of a returned set of docs from a 
        query (sum(rel_i/log2(i + 1)))

    idcg(pairList: list, k: int, query: str) : float
        Like dcg, but calculates satisfaction score based on the ideal set of returned
        urls, with the ideal set being the top k relevant documents in db

    ndcg(query_line: str) : float
        Returns the normalized dcg satisfaction score (dcg/idcg)

    parseQ(query_line: str) : list
        Parses a query line, grabbing only the query and returned urls. Returns a list
        with list[0] being the query and list[1:n] being the urls

    relCount(pairList: list, thresh: int) : int
        Uses the list returned by parseQ to determine how many relevant urls were
        returned
    """

    def __init__(self, judgement_file):
        # dictionary that will hold judgement file data 
        self.judgements = {}

        with open(judgement_file, 'r') as fd:
            for line in fd:
                tmp = line.split()
                query = tmp[0]
                url = tmp[1]
                judgement = tmp[2]

                # associate new dictionary with query if it hasn't been entered yet
                if query not in self.judgements:
                    self.judgements[query] = {}

                self.judgements[query][url] = int(judgement)


    def prec(self, query_line: str, thresh: int) -> float:
        
        pairList = self.parseQ(query_line)
        query = pairList.pop(0)
        relCount = self.relCount(query, pairList, thresh)

        return relCount/len(pairList)


    def recall(self, query_line: str, thresh: int) -> float:
       
        pairList = self.parseQ(query_line)
        query = pairList.pop(0)
        rel = self.relCount(query, pairList, thresh)
        total = 0

        for url in self.judgements[query]:
            if self.judgements[query][url] >= thresh:
                total += 1

        # catching division by zero
        if total == 0:
            return 0
        else:
            return rel/total

    def rr(self, query_line: str, thresh: int) -> float:
            
        pairList = self.parseQ(query_line)
        query = pairList.pop(0)
        i = 0 

        # catching dict key error
        if query not in self.judgements:
            return 1 

        for url in pairList:
            i += 1
            
            if url in self.judgements[query]:
                if self.judgements[query][url] >= thresh:
                        return 1/i
            else:
                continue

        return 0

    def f1_score(self, query_line: str, thresh: int) -> float:

        alpha = 0.5

        # catching division by zero
        try:
            invRecall = 1/self.recall(query_line, thresh)
            invPrec = 1/self.prec(query_line, thresh)
        except:
            return 0
        
        score = 1/(alpha*invPrec + (1 - alpha)*invRecall)

        return score

    def dcg(self, pairList: list, query: str) -> float:        
        
        i = 1
        total = 0
        
        # loop through urls present in pairList and sum up their judgement scores vs position 
        # in list
        for url in pairList:
            if url in self.judgements[query]:
                rel  = self.judgements[query][url]
                denom = math.log2(i + 1)
                total += rel/denom

            i += 1
        
        return total

    def idcg(self, pairList: list, k: int, query: str) -> float:        
        
        judgeList = []
        i = 1
        total = 0

        for url in self.judgements[query]:
            judgeList.append(self.judgements[query][url])
            
        # sort list to ideal order (descending order)
        judgeList.sort(reverse = True)

        # loop/sum until k calculations have been made
        for judgement in judgeList[:k]:
            rel = judgement
            denom = math.log2(i + 1)
            total += rel/denom

            i += 1

        return total

    def ndcg(self, query_line: str) -> float:

        pairList = self.parseQ(query_line)
        query = pairList.pop(0)

        # used by idcg 
        k = len(pairList)
        
        # preventing judgements dictionary key error
        if query not in self.judgements:
            return 0
        
        # calculate the (i)dcg scores, and then return their ratio
        dcg = self.dcg(pairList, query)
        idcg = self.idcg(pairList, k, query)
        
        # catching division by zero
        if idcg == 0:
            return 0
        
        return dcg/idcg

    def parseQ(self, query_line: str) -> list:
        tmp = query_line.split()
        query = tmp[0]

        # first index of pairList set to query portion of query_line
        pairList = [query]

        # urls start at index 3 of query_line and end at index 13
        for url in tmp[3:13]:
            pairList.append(url)
        
        return pairList

    def relCount(self, query: str, pairList: list, thresh: int) -> int:

        relCount = 0

        # preventing judgements dictionary key error
        if query not in self.judgements:
            return relCount

        # increment relevant count if the given url has a judgement score
        # greater than or equal to thresh
        for url in pairList:
            if url in self.judgements[query] and self.judgements[query][url] >= thresh: 
                relCount += 1

        return relCount

