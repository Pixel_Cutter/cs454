Name: Jared Diamond
HW: 3
Class: CS454
Professor: Ben McCamish

Run intructions:
	To use this class, import it into your project and then create a Ranking object:

		import assignment3 as a3

		ranking_object = a3.Ranking("relevance_judgements-v1.txt")

		# after object creation, you can use the Ranking methods and save their values to variables
		prec = ranking_object.prec(query, thresh)
        recall = ranking_object.recall(query, thresh)
        f1 = ranking_object.f1_score(query, thresh)
        rr = ranking_object.rr(query, thresh)
        ndcg = ranking_object.ndcg(query)



