Name: Jared Diamond
Assignment: HW4
Class: CS454
Professor: Ben McCamish

File contained in zip:
	main.py				The main program
	data/dishes.json	The raw json data for the database
	requirements.txt	The needed requirements for main.py
	index/				The database directory

Description:
This is a program that takes a json file (data/dishes.json) as input
and then builds a searchable database over the data. The database and 
searchable index are created by the python library 'whoosh.' 

Run/Search Instructions:
	0)	In the same directory as main.py, data, and index, create a virtual environment
		and then run 'pip3 install -r requirements.txt)
	1)	Run Command 'python3 main.py'
	2)	You will see a series of prompts once the program starts. The first prompts
		will ask you for configurations.
	3)	You can now search the database by entering queries into the query prompts.
		You can also enter 'q' to quit the program or 'CONFIG' to change search/result 
		behavior

NOTE:
I ommitted the image path field (found in dishes.json tuples) from the search engine
because they are only relevant if all images are on your machine. Since the total
size of all images is over 3 GB, I deemed it not practical to include them in this 
zipfile.
