#!/bin/python3

"""
Name: Jared Diamond
Assignment: HW4
Class: CS454
Professor: Ben McCamish

This is a program that takes a json file (data/dishes.json) as input
and then builds a searchable database over the data. The database and 
searchable index are created by the python library 'whoosh.' To start
searching the database, simply run the command 'python3 main.py' while
in the same directory as main.py and the needed directories: data and index.
You will see a series of prompts once the program starts. The first prompt
will ask you for configurations. You can start querying the database, once
the series of configuration prompts are completed.
"""

import os.path
import json
import textwrap
from whoosh.index import create_in, open_dir
from whoosh.qparser import MultifieldParser as mfp
from whoosh.qparser import OrGroup
from whoosh.fields import *

# search session user configurations
# 'topN' = how many results to display after a search
# 'searchType' = type of search (AND = conjunctive, OR = disjunctive)
usrConfig = {"topN": 10, "searchType": "AND"}

def isQuit(usrInput):
    if usrInput.lower() == 'q':
        print("Goodbye")
        exit()
    
    return

# Returns a schema that will be used by the indexer 'ix' 
# dish name, description, and type will be indexed over and stored
# dish list url links will be simply stored

def getSchema():
    schema = Schema(
            id=ID(stored=True),
            title=TEXT(stored=True),
            description=TEXT(stored=True),
            type=TEXT(stored=True),
            link=STORED) 

    return schema

# Prints out all results returned from a searched query to the console.
# Printed results will be displayed in a "card-like" form:
#
#   **************************************************************
#
#                               1/10
#   ID: 216
#   Name: Sekacz
#   Type of Dish: Cake
#   Source URL: https://....
#   Description: sponge cake with chocolate
#
#   **************************************************************

def printResults(resultDict):
    print("\n")
    totalResults = f"{resultDict['total-results']} results for '{resultDict['query']}'"
    title = f"< There are {totalResults}, here are the top {resultDict['topN']} >\n"
    print(title.center(80))

    i = 1

    for result in resultDict['results']:
        print(80*"*" + "\n")
        resultNum = f"{i}/{str(resultDict['topN'])}"
        print(resultNum.center(80) + "\n")
        print(f"ID: {str(result['id'])}")
        print(f"Name: {result['name'].title()}")
        print(f"Type of Dish: {result['type'].title()}")
        print(f"Associated List: {result['link']}")
        print(textwrap.fill("Description: " + result['description'], width=80) + "\n")
        i += 1

    print(80*"*" + "\n")

# Configures the behavior of the search engine. Configurations are stored in usrConfig, 
# and are used by getResults() serve up the appropriate query results. The configs can
# be changed at any time by typing 'CONFIG' into the query prompt

def getConfigs():
    searchType = input("Enter 'AND' for conjunctive search, or 'OR' for disjunctive search: ")
    isQuit(searchType)
    
    # while searchType != AND or OR, continue to prompt user 
    while searchType != 'AND' and searchType != 'OR':
        searchType = input("Please, only enter 'AND' or 'OR': ")
        isQuit(searchType)            

    # Search behavior set
    usrConfig['searchType'] = searchType

    limit = input("Enter the amount of results you want displayed from a search: ")

    # while limit != a positive integer, continue to prompt user
    while True:
        isQuit(limit)
        try:
            limit = int(limit)
            if limit < 0:
                limit = input("Enter only positive integers: ")
                continue
            break
        except:
            limit = input("Enter only positive integers: ")
            continue

    # Amount of results to return set
    usrConfig['topN'] = limit
    # done with configs, continue on to getting queries
    print("Done with setting up configs")
    print("Type 'CONFIG' into query prompt any time you want to change settings\n")

# Creates a new Whoosh database and then stores an indexed json file

def indexNewDB(schema, indexdir, dataFile):
    # use schema to build index
    ix = create_in(indexdir, schema)
    writer = ix.writer()

    with open(dataFile, 'r') as fd:
        data = json.load(fd)

    for dish in data['dishes']:
        writer.add_document(
                id=str(dish['id']),
                title=dish['name'],
                description=dish['description'],
                type=dish['type'],
                link=dish['srcPage'])
    
    # commit changes additions to whoosh database 
    writer.commit()
    return ix

# Takes a query and searches over the indexed database for query terms.
# Hits will be ranked and then a dictionary containing query stats will be 
# returned. Besides the results, stats such as total relevant docs in database, 
# query, and total amount of returned docs will also be returned

def getResults(userQuery, ix):
    # dictionary that will hold all search result stats
    resultDict = {"query": userQuery, "results": []}

    with ix.searcher() as searcher:
        # Conjunctive search if usrConfig['searchType'] == AND
        # Disjunctive search if usrConfig['searchType'] == OR
        # Both search types search over multiple indexed fields:
        #   id, title, description, and type
        if usrConfig["searchType"] == "AND":
            parser = mfp(['id', 'title', 'description', 'type'], schema=ix.schema)
        elif usrConfig["searchType"] == "OR":
            parser = mfp(['id', 'title', 'description', 'type'],
                    schema=ix.schema,
                    group=OrGroup)

        # parse query and search the database
        query = parser.parse(userQuery)
        results = searcher.search(query, limit=usrConfig["topN"])

        # for every result returned, append a dict object containing all
        # indexed/stored (according to schema) data to resultDict['results']
        for result in results:
            resultDict['results'].append({
                "id": result['id'],
                "name": result['title'],
                "description": result['description'],
                "link": result['link'],
                "type": result['type']
                })

        # total number of relevant docs in database
        resultDict['total-results'] = len(results)
        # how many results were returned 
        resultDict['topN'] = len(resultDict['results'])

    return resultDict

# Will continue to prompt user for a new query until they input 'q'
# Each query will be searched for in the index and it's returned results will
# be printed out to the console

def getQueries(ix):
    while True:
        userQuery = input("Enter a query (or type 'q' to quit): ")
        isQuit(userQuery)

        if userQuery == 'CONFIG':
            getConfigs()
            continue
        
        # query is searched for in index and its return results are printed
        # out to the console
        printResults(getResults(userQuery, ix))

def main():
    
    dataFile = "data/dishes.json"
    indexdir = "index"

    # checking for index directory
    # if it doesn't exist: make a new one
    # else: open existing one
    if not os.path.exists(indexdir):
        os.mkdir(indexdir)
        ix = indexNewDB(getSchema(), indexdir, dataFile)
    else:
        ix = open_dir(indexdir)

    # program introduction 
    welcome = "Welcome to my search engine"
    print(welcome.center(80))
   
    # grab configurations for search session
    configAlert = "Let's setup some configurations before we get started"
    print(configAlert.center(80) + "\n")
    getConfigs()

    # time to prompt for queries
    qAlert = "Enter queries when prompted, or type 'quit' / 'q' to quit the program"
    print(qAlert.center(80) + "\n")
    
    print(80*"*")

    getQueries(ix)

main()
