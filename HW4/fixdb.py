import json

with open("olddishes.json", "r") as fd:
    data = json.load(fd)

grr = {
            "cakes": "cake",
            "bacon dishes": "bacon",
            "breakfast drinks": "breakfast drink",
            "street foods": "street food",
            "raw fish dishes": "raw fish",
            "hors d'oeuvre": "hors d'oeuvre",
            "fermented foods": "fermented",
            "pasta": "pasta",
            "breads": "bread",
            "hamburgers": "hamburger",
            "egg dishes": "egg",
            "fried dough foods": "fried dough",
            "choux pastry dishes": "choux pastry",
            "cookies": "cookie",
            "German desserts": "german dessert",
            "pastries": "pastry",
            "Polish desserts": "polish dessert",
            "salads": "salad",
            "sandwiches": "sandwich",
            "soups": "soup",
            "stews": "stew",
            "rolled foods": "rolled"
        }

i = 0
newdb = {"total-dishes": 0, "dishes": []}

# adding type and from-list fields, making one list, converting fields to lowercase, 
# and changing id number system
for og, fixed in grr.items():
    for dish in data[og]:
        dish['type'] = fixed
        dish['id'] = i
        dish['name'] = dish['name'].lower()
        dish['description'] = dish['description'].lower()
        newdb['dishes'].append(dish)
        i += 1
        newdb['total-dishes'] += 1

with open('data/newdishes.json', 'w') as fd:
    json.dump(newdb, fd, indent=4)

